package com.homework.viwatspringbootwiththymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ViwatSpringBootWithThymeleafApplication {

    public static void main(String[] args) {
        SpringApplication.run(ViwatSpringBootWithThymeleafApplication.class, args);
    }

}
