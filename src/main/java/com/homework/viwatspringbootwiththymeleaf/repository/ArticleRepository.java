package com.homework.viwatspringbootwiththymeleaf.repository;

import com.homework.viwatspringbootwiththymeleaf.model.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleRepository extends JpaRepository<Article,Integer> {
}
